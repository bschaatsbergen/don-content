# Content for developer.overheid.nl

This repository is the source of the API's and repositories that are shown on [developer.overheid.nl](https://developer.overheid.nl).

## Contribute

You can contribute by forking this repository and creating a merge request with
your changes. Before contributing we recommend that you read
[CONTRIBUTING.md](CONTRIBUTING.md).

## Sync

At regular intervals the content of this repository is synced to [developer.overheid.nl](https://developer.overheid.nl)

## Licence

Copyright © VNG Realisatie 2022

[Licensed under the EUPLv1.2](LICENCE.md)
